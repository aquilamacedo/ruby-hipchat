require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = FileList['./spec/**/*_spec.rb'] - FileList['./spec/hipchat_api_v1_spec.rb','./spec/hipchat_api_v2_spec.rb']
end
